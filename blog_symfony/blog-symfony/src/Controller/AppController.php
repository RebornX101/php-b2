<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{
    /**
     * @Route("/app", name="app")
     */
    public function index()
    {
        return $this->render('app/index.html.twig', [
            'controller_name' => 'Sam',
        ]);
    }

    /**
     *  @Route("/about", name="about")
     */
    public function about_show() {

        $userName = "Reborn";
        return $this->render('app/about.html.twig', [
            'firstName' => "Sam"
            ]);
    }

}
