<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article")
     */
    public function index(ArticleRepository $articleRepo)
    {
        /**
        $articleList = [];

        $article1 = new Article();
        $article1->setTitle('Titre 1')
                ->setDescription('Lorem')
                ->setCreatedAt(new \DateTime());

                
        $article2 = new Article();
        $article2->setTitle('Titre 2')
                ->setDescription('Lorem')
                ->setCreatedAt(new \DateTime());

        dd($article1);
        array_push($articleList, $article1, $article2);

        $articleManager = $this->getDoctrine()->getManager();
        $articleManager->persist($article1);
        $articleManager->persist($article1);

        $articleManager->flush();
        
        dd($articleList);
        **/
        $articleList = $articleRepo->findAll();

        return $this->render('article/index.html.twig', [
            'articleList' => $articleList,
            ]);
    }
}
